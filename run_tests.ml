  
let () =


  (* H2O / cc-pVDZ *)
  let test_integrals_water, test_guess_water = 
    let basis_file   = "test_files/cc-pvdz"
    and nuclei_file  = "test_files/water.xyz"
    in

    let simulation_closed_shell =
      Simulation.of_filenames ~charge:0 ~multiplicity:1 ~nuclei:nuclei_file basis_file
    in

    let ao_basis = 
      Simulation.ao_basis simulation_closed_shell
    in
    (fun () -> AOBasis.test_case "water" ao_basis),
    (fun () -> Guess.test_case ao_basis);
  in


  (* LiH / cc-pVTZ *)
  let test_integrals_lih =
    let basis_file   = "test_files/cc-pvdz"
    and nuclei_file  = "test_files/lih.xyz"
    in

    let simulation_closed_shell =
      Simulation.of_filenames ~range_separation:0.5 ~charge:0 ~cartesian:true ~multiplicity:1 ~nuclei:nuclei_file basis_file
    in

    let ao_basis = 
      Simulation.ao_basis simulation_closed_shell
    in
    (fun () -> AOBasis.test_case "lih" ao_basis)
  in

  Alcotest.run "Unit tests" [
      "Util", Util.test_case ();
      "Bitstring", Bitstring.test_case ();
      "Spindeterminant", Spindeterminant.test_case ();
      "Determinant", Determinant.test_case ();
      "Excitation", Excitation.test_case ();
      "Sparse vectors", Vector.test_case ();
      "Sparse matrices", Matrix.test_case ();
      "Cholesky", Cholesky.test_case ();
      "Guess water", test_guess_water ();
      "AO Integrals water", test_integrals_water ();
      "AO Integrals LiH", test_integrals_lih ();
    ]



