(** Data structure describing a couple of atomic shells pairs.

An atomic shell pair couple is the cartesian product between two sets of functions, one
set over electron one and one set over electron two. Both sets are atomic shell
pairs.

These are usually called {e shell quartets} in the literature, but we prefer to use
{e pair} for two functions with the same electron, and {e couple} for two functions
acting on different electrons, since they will be coupled by a two-electron operator.


*)

type t 


val make : ?cutoff:float -> AtomicShellPair.t -> AtomicShellPair.t -> t option
(** Creates an atomic shell pair couple using two atomic shell pairs.
*)

val ang_mom : t -> AngularMomentum.t
(** Total angular momentum of the shell pair couple: sum of the angular momenta of
    all the shells.  *)


val atomic_shell_a : t -> AtomicShell.t
(** Returns the first atomic shell of the first shell pair. *)

val atomic_shell_b : t -> AtomicShell.t
(** Returns the second atomic shell of the first shell pair. *)

val atomic_shell_c : t -> AtomicShell.t
(** Returns the first atomic shell of the second shell pair. *)

val atomic_shell_d : t -> AtomicShell.t
(** Returns the second atomic shell of the second shell pair. *)


val atomic_shell_pair_p : t -> AtomicShellPair.t
(** Returns the first atomic shell pair that was used to build the shell pair. *)

val atomic_shell_pair_q : t -> AtomicShellPair.t
(** Returns the second atomic shell pair that was used to build the shell pair. *)

val monocentric : t -> bool
(** True if all four atomic shells have the same center. *)

val contracted_shell_pair_couples : t -> ContractedShellPairCouple.t list
(** Returns the list of significant contracted shell pair couples. *)

val zkey_array : t -> Zkey.t array
(** Returns the array of {!Zkey.t} relative to the four shells of the
    shell pair couple.
*)

val norm_scales : t -> float array
(** Scaling factors of normalization coefficients inside the shell. The
    ordering is the same as {!zkey_array}.
*)


