(** Multipole atomic integrals:

{% $$ \langle \chi_i | x | \chi_j \rangle $$ %}
{% $$ \langle \chi_i | y | \chi_j \rangle $$ %}
{% $$ \langle \chi_i | z | \chi_j \rangle $$ %}
{% $$ \langle \chi_i | x^2 | \chi_j \rangle $$ %}
{% $$ \langle \chi_i | y^2 | \chi_j \rangle $$ %}
{% $$ \langle \chi_i | z^2 | \chi_j \rangle $$ %}

*)

open Lacaml.D

type t

val matrix_x : t -> Mat.t
(** {% $$ \langle \chi_i | x | \chi_j \rangle $$ %} *)

val matrix_y : t -> Mat.t
(** {% $$ \langle \chi_i | y | \chi_j \rangle $$ %} *)

val matrix_z : t -> Mat.t
(** {% $$ \langle \chi_i | z | \chi_j \rangle $$ %} *)

val matrix_x2 : t -> Mat.t
(** {% $$ \langle \chi_i | x^2 | \chi_j \rangle $$ %} *)

val matrix_y2 : t -> Mat.t
(** {% $$ \langle \chi_i | y^2 | \chi_j \rangle $$ %} *)

val matrix_z2 : t -> Mat.t
(** {% $$ \langle \chi_i | z^2 | \chi_j \rangle $$ %} *)

val matrix_x3 : t -> Mat.t
(** {% $$ \langle \chi_i | x^3 | \chi_j \rangle $$ %} *)

val matrix_y3 : t -> Mat.t
(** {% $$ \langle \chi_i | y^3 | \chi_j \rangle $$ %} *)

val matrix_z3 : t -> Mat.t
(** {% $$ \langle \chi_i | z^3 | \chi_j \rangle $$ %} *)

val matrix_x4 : t -> Mat.t
(** {% $$ \langle \chi_i | x^4 | \chi_j \rangle $$ %} *)

val matrix_y4 : t -> Mat.t
(** {% $$ \langle \chi_i | y^4 | \chi_j \rangle $$ %} *)

val matrix_z4 : t -> Mat.t
(** {% $$ \langle \chi_i | z^4 | \chi_j \rangle $$ %} *)

val of_basis : Basis.t -> t
