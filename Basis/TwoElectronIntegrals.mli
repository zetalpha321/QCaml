(** Two-electron integrals with an arbitrary operator, with a functorial interface            
    parameterized by the fundamental two-electron integrals. 

    {% $(00|00)^m = \int \int \phi_p(r1) \hat{O} \phi_q(r2) dr_1 dr_2 $ %} : Fundamental two-electron integral

*)


module type TwoEI_structure =
  sig
val name : string
(** Name of the kind of integrals, for printing purposes. *)

val class_of_contracted_shell_pair_couple :
  basis:Basis.t -> ContractedShellPairCouple.t -> float Zmap.t                                              
(** Returns an integral class from a couple of contracted shells.
    The results is stored in a Zmap.
*)
  end



module Make : functor (T : TwoEI_structure) -> 
    sig
      include module type of FourIdxStorage

      val filter_contracted_shell_pairs :
        ?cutoff:float -> basis:Basis.t ->
        ContractedShellPair.t list -> ContractedShellPair.t list
      (** Uses Schwartz screening on contracted shell pairs. *)

      val of_basis : Basis.t -> t
      (** Compute all ERI's for a given {!Basis.t}. *)

    end

