(** Contains powers of x, y and z describing the polynomials in atomic basis sets. *)

type t = private {
    x   : int ;
    y   : int ;
    z   : int ;
    tot : int ; (* x + y + z *)
  }


val of_int_tuple : int * int * int -> t
(** Example:
  [of_int_tuple (2,3,1) -> { x=2 ; y=3 ; z=1 ; tot=6 }]
  @raise Invalid_argument if x, y or z < 0.
  *)


val to_int_tuple : t -> int * int * int 
(** Example:
  [to_int_tuple { x=2 ; y=3 ; z=1 ; tot=6 } -> (2,3,1) ]
  *)


val get  : Coordinate.axis -> t -> int
(** Example:

  [Powers.get Coordinate.Y { x=2 ; y=3 ; z=1 ; tot=6 } -> 3]

  *)


val incr : Coordinate.axis -> t -> t 
(** Returns a new {!Powers.t} with the power on the given axis incremented.

  Example:

  {[
    Powers.incr Coordinate.Y { x=2 ; y=3 ; z=1 ; tot=6 } ->
      { x=2 ; y=4 ; z=1 ; tot=7 }
  ]}

  *)


val decr : Coordinate.axis -> t -> t
(** Returns a new {!Powers.t} with the power on the given axis decremented.
  As opposed to {!of_int_tuple}, the values may become negative.

  Example:

  {[
    Powers.incr Coordinate.Y { x=2 ; y=3 ; z=1 ; tot=6 } ->
      { x=2 ; y=2 ; z=1 ; tot=5 }
  ]}

  *)

