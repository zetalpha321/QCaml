(** Number of {% $\alpha$ %} and {% $\beta$ %} electrons *)

type t = {
  n_alfa : int ;
  n_beta : int ;
  multiplicity : int;
}


let make ?multiplicity:(multiplicity=1) ?charge:(charge=0) nuclei =
  let positive_charges =
    Array.fold_left (fun accu (e, _) -> accu + Charge.to_int (Element.to_charge e) )
      0 nuclei
  in
  let negative_charges = charge - positive_charges in
  let n_elec = - negative_charges in
  let n_beta = ((n_elec - multiplicity)+1)/2 in
  let n_alfa = n_elec - n_beta in
  let result = { n_alfa ; n_beta ; multiplicity } in
  if multiplicity <> (n_alfa - n_beta)+1 then
    invalid_arg (__FILE__^": make");
  result

let charge e =
  - (e.n_alfa + e.n_beta)
  |> Charge.of_int

let n_alfa t = t.n_alfa
let n_beta t = t.n_beta
let n_elec t = t.n_alfa + t.n_beta
let multiplicity t = t.multiplicity
