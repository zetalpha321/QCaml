(** Chemical elements.  *)

exception ElementError of string

type t =
|X
|H                                                 |He
|Li|Be                              |B |C |N |O |F |Ne
|Na|Mg                              |Al|Si|P |S |Cl|Ar
|K |Ca|Sc|Ti|V |Cr|Mn|Fe|Co|Ni|Cu|Zn|Ga|Ge|As|Se|Br|Kr
|Rb|Sr|Y |Zr|Nb|Mo|Tc|Ru|Rh|Pd|Ag|Cd|In|Sn|Sb|Te|I |Xe
                           |Pt          


val of_string : string -> t
(** Creates an {!Element.t} from a chemical symbol or from the full name of the
  element (case insensitive).
  @raise ElementError upon failure

Examples:

{[
Element.of_string "Fe" -> Element.Fe
Element.of_string "hydrogen" -> Element.H
Element.of_string "pouet" -> ElementError "Element pouet unknown"
]}
  *)

val to_string : t -> string
(** Gets the chemical symbol of the {!Element.t} in a string.

Example:

[Element.(to_string Fe) -> "Fe"]
*)

val to_long_string : t -> string
(** Gets the full name of the {!Element.t} in a string.

Example:

[Element.(to_long_string Fe) -> "Iron"]
*)

val to_int : t -> int 
(** Convert to the atomic charge, with [int] type. *)

val of_int : int -> t
(** Create from the atomic charge, with [int] type. *)

val to_charge : t -> Charge.t
(** Convert to the atomic charge, with {!Charge.t} type. *)

val of_charge : Charge.t -> t
(** Create from the atomic charge, with {!Charge.t} type. *)

val covalent_radius : t -> NonNegativeFloat.t
(** Covalent radii of the elements, in atomic units. *)

val vdw_radius : t -> NonNegativeFloat.t
(** Van der Waals radii of the elements, in atomic units. *)

val mass : t -> Mass.t
(** Atomic mass of the elements, in atomic units. *)

val small_core : t -> int
(** Number of electrons in the small core model (all except the outermost two shells). *)

(* TODO
val large_core : t -> int
(** Number of electrons in the large core model (all except the outermost shell). *)
*)
