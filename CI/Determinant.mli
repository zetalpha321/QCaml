(** A Slater determinant is expressed as a Waller-Hartree double determinant:
{% $$
D(\mathbf{R}) = D_\alpha(\mathbf{R_\alpha}) \times D_\beta(\mathbf{R_\beta}) 
$$ %}
The {% $\alpha$ %} and {% $\beta$ %} determinants are of type [Spindeterminant.t].
*)


type t
type hole = int
type particle = int

(** {1 Accessors} *)

val alfa : t -> Spindeterminant.t
(** Get the {% $\alpha$ %} spin-determinant. *)

val beta : t -> Spindeterminant.t
(** Get the {% $\beta$ %} spin-determinant. *)

val phase : t -> Phase.t
(** Get the phase of the Slater determinant, the product of the phases of the
    spin-determinants.
*)

val is_none : t -> bool
(** Tests if a Determinant is [None]. *)


(** {1 Second quantization operators} *)

val vac : int -> t
(** Vacuum state, [vac = Some ]{% $|\rangle$ %}. The integer parameter is the size of the
    MO basis set. *)

val creation : Spin.t -> particle -> t -> t
(** [creation spin p] is the creation operator {% $a^\dagger_p$ %}. *)

val annihilation : Spin.t -> hole -> t -> t
(** [annihilation spin h] is the annihilation operator {% $a_h$ %}. *)

val single_excitation : Spin.t -> hole -> particle -> t -> t
(** Single excitation operator {% $T_h^p = a^\dagger_p a_h$ %}. *)

val double_excitation : Spin.t -> hole -> particle -> Spin.t -> hole -> particle -> t -> t
(** Double excitation operator {% $T_{hh'}^{pp'} = a^\dagger_p a^\dagger_{p'} a_{h'} a_h$ %}. *)

val degree_alfa : t -> t -> int
(** Returns degree of excitation between two determinants in the {% $\alpha$ %} spin. *)

val degree_beta : t -> t -> int
(** Returns degree of excitation between two determinants in the {% $\beta$ %} spin. *)

val degrees : t -> t -> int*int
(** Returns degrees of excitation between two determinants in {% $\alpha$ %} and
    {% $\beta$ %} spins as a pair. *)

val degree : t -> t -> int
(** Returns degree of excitation between two determinants. *)

val to_lists : t -> int list * int list
(** Converts a Slater determinant to a pair of lists of orbital indices.  *)


(** {1 Creators} *)

val of_spindeterminants : Spindeterminant.t -> Spindeterminant.t -> t
(** Creates a Slater determinant from an {% $\alpha$ %} and a {% $\beta$ %}
    [Spindeterminant.t].
*)

val of_lists : int -> int list -> int list -> t
(** Creates a Slater determinant from a two lists of orbital indices.
    The integer parameter is the size of the MO basis set. *)

val negate_phase : t -> t
(** Returns the same determinant with the phase negated. *)

val set_phase : Phase.t -> t -> t
(** Returns the same determinant with the phase set to [p]. *)

val compare : t -> t -> int
(** Comparison function for sorting *)


(** {1 Printers} *)

val pp : int -> Format.formatter -> t -> unit
(** First [int] is the number of MOs to print. *)

(** {1 Unit tests} *)

val test_case : unit -> (string * [> `Quick ] * (unit -> unit)) list
