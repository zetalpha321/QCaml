QCaml (Quantum Camel)
=====================

<img src="data/chamo_bg.png"  width="300" >

Requirements
------------

* MPI : Message Passing Interface
* BLAS/LAPACK : Linear algebra
* LaCaml : LAPACK OCaml interface
* Zarith : Arbitrary-precision integers
* gmp : GNU Multiple Precision arithmetic library
* odoc-ltxhtml : https://github.com/akabe/odoc-ltxhtml
* Alcotest : Lightweight testing framework


